import React from 'react';
import renderer from 'react-test-renderer';
import WeatherApp from "../src/WeatherApp";

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'lonely.penguin.api.base': '/api',
    })
}));

it('Weather renders correctly', () => {
    const tree = renderer
        .create(<WeatherApp/>)
        .toJSON();
    expect(tree).toMatchSnapshot();
});