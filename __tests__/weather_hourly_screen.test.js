import React from 'react';
import {mount} from 'enzyme';
import moxios from 'moxios'
import WeatherScreen from "../src/screens/WeatherScreen";
import WeatherHourlyScreen from "../src/screens/WeatherHourlyScreen";

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'lonely.penguin.api.base': '/api',
    })
}));

describe('Weather search test', () => {
    beforeEach(function () {

        moxios.install()
    })

    afterEach(function () {

        moxios.uninstall()
    })

    it('One-call 200', () => {
        moxios.stubRequest(/.*\/onecall/, {
            status: 200
        })

        const tree = mount(<WeatherHourlyScreen isLoading={false}/>);

        expect(tree.find("#chart").first()).toBeTruthy()

    })

    it('One-call 401', () => {
        moxios.stubRequest(/.*\/onecall/, {
            status: 401
        })

        const tree = mount(<WeatherHourlyScreen isLoading={true}/>);

        expect(tree.find("#progress").first()).toBeTruthy()
    })
})