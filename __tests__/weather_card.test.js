import React from 'react';
import {mount, shallow} from 'enzyme';
import moxios from 'moxios'
import WeatherCard, {epochToTime} from "../src/components/WeatherCard";

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'lonely.penguin.api.base': '/api',
    })
}));

describe('Testing weather card', () => {
    beforeEach(function () {

        moxios.install()
    })

    afterEach(function () {

        moxios.uninstall()
    })

    it('Weather request 200', () => {
        moxios.stubRequest(/.*\/weather.*/, {
            status: 200,
            data: {
                "name": "Innopolis"
            }
        })

        const tree = mount(<WeatherCard city="Innopolis"/>);

    })

    it('Weather request 401', () => {
        moxios.stubRequest(/.*\/weather.*/, {
            status: 401
        })

        const tree = mount(<WeatherCard city="Innopolis"/>);

        expect(tree.find("#progress").first()).toBeTruthy()
    })

    it('Weather epoch test', () => {

        epochToTime(1601703254)

    })
})