import React from 'react';
import {mount} from 'enzyme';
import moxios from 'moxios'
import WeatherScreen from "../src/screens/WeatherScreen";
import {ContextProps} from "../src/Context";

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'lonely.penguin.api.base': '/api',
    })
}));

describe('Weather search test', () => {
    beforeEach(function () {

        moxios.install()
    })

    afterEach(function () {

        moxios.uninstall()
    })

    it('Weather search 200', () => {
        moxios.stubRequest(/.*\/weather/, {
            status: 200
        })

        const context = {
            currentCity: "Innopolis",
            setCurrentCity: () => {},
        }

        const tree = mount(<WeatherScreen context={context}/>);

        const searchInput = tree.find("#searchInput").first();
        searchInput.simulate('change', {target: {value: 'Innopolis'}})

        const submitButton = tree.find('#searchSubmit').first()
        submitButton.simulate('click')
    })

    it('Weather search 401', () => {
        moxios.stubRequest(/.*\/weather/, {
            status: 401
        })

        const context = {
            currentCity: "Innopolis",
            setCurrentCity: () => {},
        }

        const tree = mount(<WeatherScreen context={context}/>);

        const searchInput = tree.find("#searchInput").first();
        searchInput.simulate('change', {target: {value: 'Innopolis'}})

        const submitButton = tree.find('#searchSubmit').first()
        submitButton.simulate('click')
    })
})