import React from 'react';
import renderer from 'react-test-renderer';
import WeatherApp from "../src/WeatherApp";
import WeatherScreen from "../src/screens/WeatherScreen";
import WeatherHourlyScreen from "../src/screens/WeatherHourlyScreen";
import WeatherDailyScreen from "../src/screens/WeatherDailyScreen";

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'lonely.penguin.api.base': '/api',
    })
}));

it('Weather renders correctly', () => {
    const tree = renderer
        .create(<WeatherDailyScreen/>)
        .toJSON();
    expect(tree).toMatchSnapshot();
});