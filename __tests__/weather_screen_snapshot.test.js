import React from 'react';
import renderer from 'react-test-renderer';
import WeatherApp from "../src/WeatherApp";
import WeatherScreen from "../src/screens/WeatherScreen";

jest.mock('@ijl/cli', () => ({
    getConfig: () => ({
        'lonely.penguin.api.base': '/api',
    })
}));

it('Weather renders correctly', () => {

    const context = {
        currentCity: "Innopolis",
        setCurrentCity: () => {},
    }

    const tree = renderer
        .create(<WeatherScreen context={context}/>)
        .toJSON();
    expect(tree).toMatchSnapshot();
});