const pkg = require('./package')

module.exports = {
    "apiPath": "stubs/api",
    "webpackConfig": {
        "output": {
            "publicPath": `/static/lonely.penguin/${process.env.VERSION || pkg.version}/`
        }
    },
    "navigations": {
        "repos": "/lonely.penguin"
    },
    apps: {
        "lonely.penguin": { name: 'lonely.penguin', version: process.env.VERSION || pkg.version }
    },
    config: {
        "lonely.penguin.api.base": "/api"
    },
}