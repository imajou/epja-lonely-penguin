import {createMuiTheme} from '@material-ui/core/styles';
import {deepPurple, pink, red} from '@material-ui/core/colors';

// Create a theme instance.
const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: pink.A200,
        },
        secondary: {
            main: deepPurple.A200,
        },
        error: {
            main: red.A400,
        },
    },
});

export default theme;