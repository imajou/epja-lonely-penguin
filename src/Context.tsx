import React from 'react'

export interface ContextProps {
    currentCity: string,
    setCurrentCity: React.Dispatch<React.SetStateAction<string>>,
    currentCoords: object,
    setCurrentCoords: React.Dispatch<React.SetStateAction<object>>,
}

const Context = React.createContext<ContextProps>({
    currentCity: "",
    setCurrentCity: void {},
    currentCoords: {"lat": 0, "lon": 0},
    setCurrentCoords: void {},
})
export const ContextProvider = Context.Provider
export default Context