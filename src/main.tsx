import React from 'react';
import ReactDom from 'react-dom';
import App from './WeatherApp';
import {ThemeProvider} from '@material-ui/core/styles';
import theme from "@main/Theme";
import {CssBaseline} from "@material-ui/core";

export const mount = () => {
    ReactDom.render(
        <ThemeProvider theme={theme}>
            <CssBaseline/>
            <App/>
        </ThemeProvider>,
        document.getElementById('app')
    );
};

export const unmount = () => {
    ReactDom.unmountComponentAtNode(document.getElementById('app'));
};