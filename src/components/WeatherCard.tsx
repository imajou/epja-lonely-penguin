import Card from "@material-ui/core/Card";
import React, {useState} from "react";
import {Typography} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import WeatherRequest from "@main/requests/WeatherRequest";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";
import {useHistory} from 'react-router-dom';
import {baseUrlPrefix} from "@main/Config";

const useStyles = makeStyles((theme) => ({
    root: {
        margin: '16px',
    },
    cityInfoContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'baseline'
    },
    cityInfoName: {
        marginRight: '32px'
    },
    cityInfoItem: {
        marginRight: '16px'
    },
    weatherInfoGrid: {
        marginTop: '8px'
    },
    actionButtonContainer: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '32px'
    },
    actionButton: {
        marginRight: '8px'
    }
}));

const initialState = {}

export const epochToTime = (epoch) => {
    const date = new Date(epoch * 1000);
    return String(date.getHours()) + ":" + String(date.getMinutes())
}

export default ({city}) => {
    const classes = useStyles();
    const history = useHistory();

    let [weatherData, setWeatherData] = useState(initialState);

    WeatherRequest(setWeatherData, city);

    if (weatherData == initialState) {
        return (
            <Card variant={"outlined"} className={classes.root}>
                <CardContent>
                    <CircularProgress id="progress"/>
                </CardContent>
            </Card>
        )
    }

    return (
        <Card variant={"outlined"} className={classes.root}>
            <CardContent>
                <div className={classes.cityInfoContainer}>
                    <Typography id="weatherCityName" className={classes.cityInfoName} variant="h3" component="h1"
                                noWrap>
                        {weatherData['name']}
                    </Typography>
                    <Typography className={classes.cityInfoItem} variant="body1" component="p">
                        Sunrise: {epochToTime(weatherData['sys']['sunrise'])}
                    </Typography>
                    <Typography className={classes.cityInfoItem} variant="body1" component="p">
                        Sunset: {epochToTime(weatherData['sys']['sunset'])}
                    </Typography>
                </div>
                <Grid container spacing={4} className={classes.weatherInfoGrid}>
                    <Grid item>
                        <Typography variant="h2" component="h2">
                            {weatherData['main']['temp']} °C
                        </Typography>
                        <Typography variant="body1" component="p">
                            Feels like: {weatherData['main']['feels_like']} °C
                        </Typography>
                    </Grid>
                    <Grid item>

                        <Typography variant="body1" component="p">
                            Wind: {weatherData['wind']['speed']} m/s ({weatherData['wind']['deg']}°)
                        </Typography>
                        <Typography variant="body1" component="p">
                            Pressure: {weatherData['main']['pressure']} kPa
                        </Typography>
                        <Typography variant="body1" component="p">
                            Humidity: {weatherData['main']['humidity']} %
                        </Typography>
                    </Grid>
                </Grid>
                <div className={classes.actionButtonContainer}>
                    <Button className={classes.actionButton}
                            onClick={() => {
                                history.push(`${baseUrlPrefix}/hourly`)
                            }}>Hourly forecast</Button>
                    <Button className={classes.actionButton}
                            onClick={() => {
                                history.push(`${baseUrlPrefix}/daily`)
                            }}>Daily forecast</Button>
                </div>
            </CardContent>
        </Card>
    );
}