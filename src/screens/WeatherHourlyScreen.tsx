import * as React from 'react';
import {useContext, useState} from 'react';
import Paper from '@material-ui/core/Paper';
import {
    ArgumentAxis,
    Chart,
    Legend,
    LineSeries,
    Title,
    Tooltip,
    ValueAxis,
} from '@devexpress/dx-react-chart-material-ui';
import Context from "@main/Context";
import WeatherOneCallRequest from "@main/requests/WeatherOneCallRequest";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CircularProgress from "@material-ui/core/CircularProgress";
import {EventTracker} from "@devexpress/dx-react-chart";
import Container from "@material-ui/core/Container";


const initialState = {};


export default (props) => {
    const context = useContext(Context);
    const [weatherOneCallData, setWeatherOneCallData] = useState(initialState);
    const [isLoading, setLoading] = useState(props.isLoading ? props.isLoading : true)

    WeatherOneCallRequest(setLoading, setWeatherOneCallData, context.currentCoords["lat"], context.currentCoords["lon"]);

    if (isLoading) {
        return (
            <Card variant={"outlined"}>
                <CardContent>
                    <CircularProgress id="progress"/>
                </CardContent>
            </Card>
        )
    }

    return (
        <Container>
            <Paper id='chart'>
                <Chart data={weatherOneCallData["hourly"].slice(0, 12)}>
                    <Title text={`Hourly weather forecast in ${context.currentCity}`}/>
                    <ArgumentAxis/>
                    <ValueAxis/>
                    <LineSeries name="Temperature" valueField="temp" argumentField="dt_locale_time"/>
                    <LineSeries name="Feels like" valueField="feels_like" argumentField="dt_locale_time"/>
                    <EventTracker/>
                    <Tooltip/>
                    <Legend/>
                </Chart>
            </Paper>
        </Container>
    )
};