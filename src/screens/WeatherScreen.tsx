import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {Container} from "@material-ui/core";
import React, {useState} from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import WeatherCard from "@main/components/WeatherCard";

const baseCity = "Innopolis";

const useStyles = makeStyles((theme) => ({
    searchForm: {
        display: 'flex',
        flexDirection: 'column',
        padding: '16px'
    },
}));

export default (props) => {
    const classes = useStyles();
    let [query, setQuery] = useState(props.context.currentCity)

    return (
        <Container maxWidth={"sm"}>
            <form className={classes.searchForm} noValidate>
                <TextField
                    onChange={(event) => {
                        setQuery(event.target.value)
                    }}
                    value={query}
                    variant="outlined"
                    placeholder={props.context.currentCity}
                    margin="normal"
                    autoFocus
                    id="searchInput"
                />
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    onClick={(event => {
                        event.preventDefault();
                        props.context.setCurrentCity(query)
                    })}
                    id="searchSubmit"
                >
                    Search
                </Button>
            </form>
            <WeatherCard city={props.context.currentCity}/>
        </Container>
    )
}