import React, {useEffect} from 'react';

import {apiBaseUrl, apiKey} from '../Config'

const requestParams = (lat, lon) => ({
    params: {
        "lat": lat,
        "lon": lon,
        "appid": apiKey,
        "units": 'metric'
    }
})

export default (setLoading, setWeatherOneCallData, lat, lon) => {
    const axios = require('axios');
    useEffect(() => {
        axios.get(`${apiBaseUrl}/onecall`, requestParams(lat, lon))
            .then(function (response) {
                response.data["hourly"].map((item, index) => {
                    item["dt_locale_time"] = new Date(item["dt"] * 1000).toLocaleTimeString();
                });
                response.data["daily"].map((item, index) => {
                    item["temp_day"] = item["temp"]["day"];
                    item["temp_night"] = item["temp"]["night"];
                    item["dt_locale_date"] = new Date(item["dt"] * 1000).toLocaleDateString();
                })

                setWeatherOneCallData(response.data);
                setLoading(false);
            })
            .catch(function (error) {
                console.log(error);
            })
    }, [lat, lon])
}