import React, {useContext, useEffect} from 'react';

import {apiBaseUrl, apiKey} from '../Config'
import Context from "@main/Context";

const requestParams = (city) => ({
    params: {
        "q": city,
        "appid": apiKey,
        "units": 'metric'
    }
})

export default (setWeatherData, city) => {
    const axios = require('axios');

    const context = useContext(Context)

    useEffect(() => {
        axios.get(`${apiBaseUrl}/weather`, requestParams(city))
            .then(function (response) {
                setWeatherData(response.data);
                context.setCurrentCoords({"lat": response.data["coord"]["lat"], "lon": response.data["coord"]["lon"]})
            })
            .catch(function (error) {
                console.log(error);
            })
    }, [city])
}
