import React, {useState} from 'react';
import {Box} from "@material-ui/core";
import AppBar from "@main/components/Navigation/AppBar";
import WeatherScreen from "@main/screens/WeatherScreen";
import WeatherHourlyScreen from "@main/screens/WeatherHourlyScreen";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {baseUrlPrefix} from "@main/Config";
import {ContextProps, ContextProvider} from "@main/Context";
import WeatherDailyScreen from "@main/screens/WeatherDailyScreen";

const packageJson = require('../package.json');

export const version = packageJson.version

const initialCurrentCity = "Innopolis";
const initialCurrentCoords = {"lat": 55.75, "lon": 48.74}
const initialCurrentCoordsPrimitive = 0.0


export default () => {

    const [currentCity, setCurrentCity] = useState(initialCurrentCity);
    const [currentCoords, setCurrentCoords] = useState(initialCurrentCoords);


    const context: ContextProps = {
        currentCity: currentCity,
        setCurrentCity: setCurrentCity,
        currentCoords: currentCoords,
        setCurrentCoords: setCurrentCoords,
    }

    return (
        <ContextProvider value={context}>
            <Box>
                <AppBar/>
                <Router>
                    <Switch>
                        <Route path={`${baseUrlPrefix}/`} exact>
                            <WeatherScreen context={context}/>
                        </Route>
                        <Route path={`${baseUrlPrefix}/hourly`}>
                            <WeatherHourlyScreen/>
                        </Route>
                        <Route path={`${baseUrlPrefix}/daily`}>
                            <WeatherDailyScreen/>
                        </Route>
                    </Switch>
                </Router>
            </Box>
        </ContextProvider>

    )
}