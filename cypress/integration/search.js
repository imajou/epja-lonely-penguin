describe('Search test', () => {
    it('tests search', () => {
        cy.visit('http://localhost:8099')
        cy.get('#weatherCityName').should('have.text', 'Innopolis')
        cy.get('#searchInput').type("Moscow")
        cy.get('#searchSubmit').click()
        cy.get('#weatherCityName').should('have.text', 'Moscow')
    })
})