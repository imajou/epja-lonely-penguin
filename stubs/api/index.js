const router = require('express').Router();

router.get('/weather', (req, res) => {
    if (req.query["q"] === "Moscow") {
        res.send(require("./weather/weather_moscow.json"));
    } else {
        res.send(require("./weather/weather_innopolis.json"));
    }
});

router.get('/onecall', (req, res) => {
    if (req.query["q"] === "Moscow") {
        res.send(require("./weather_onecall/weather_onecall_moscow.json"));
    } else {
        res.send(require("./weather_onecall/weather_onecall_innopolis.json"));
    }
});

module.exports = router;