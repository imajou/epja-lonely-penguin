module.exports = {
    'moduleNameMapper': {
        '@main/(.*)': '<rootDir>/src/$1'
    },
    setupFiles: [
        '<rootDir>/jest.setup.js'
    ],
    coverageThreshold: {
        global: {
            statements: 80,
            branches: 80,
            functions: 80,
            lines: 80,
        },
    }
};